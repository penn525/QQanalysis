import time
from PIL import Image

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait


class QZoneSpider():
    def __init__(self):
        self.URL = 'https://qzone.qq.com'
        self.__driver = QZoneSpider.init_driver()

    @staticmethod
    def init_driver():
        """
        初始化浏览器对象
        """
        opts = Options()
        opts.add_argument('--headless')
        return webdriver.Chrome(options=opts)

    def qzone_login(self):
        """
        保存登录二维码，并让客户进行扫码登录
        """
        self.__driver.get(self.URL)
        self.__driver.save_screenshot('../images/qqlogin.png')
        im = Image.open('../images/qqlogin.png')
        im.show()
        # 检查某个元素，确认是否登录成功
        time.sleep(15)
        # print(self.__driver.current_url)

    def get_visitor_data(self):
        """
        获取所有的访客数据，保存在json对象中
        """
        visitors = self.__driver.find_elements_by_xpath(
                "//div[@id='QM_3_Body']//li[@class='user-item']")

        for visitor in visitors:
            name = visitor.find_element_by_xpath(
                    './a/img').get_attribute('alt')[:-3]
            date = visitor.find_element_by_class_name('date').text
            print(name, date)

    def next_page(self):
        """获取下一页访问者数据"""
        try:
            npage = self.__driver.find_element_by_xpath(
                    "//div[@id='QM_3_pager']/a[@class='next-page']")
            npage.click()
            time.sleep(1)
        except Exception as e:
            print(e)
            print('Last Page!')
            self.__driver.quit()
            return False
        return True

    def start(self):
        self.qzone_login()
        self.get_visitor_data()
        while self.next_page():
            self.get_visitor_data()


if __name__ == "__main__":
    spider = QZoneSpider()
    spider.start()
